package com.example.kafka.springbootkafkakuzmenko;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootKafkaKuzmenkoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootKafkaKuzmenkoApplication.class, args);
	}

}
